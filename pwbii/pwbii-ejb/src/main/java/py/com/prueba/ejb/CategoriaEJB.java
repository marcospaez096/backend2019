/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.prueba.ejb;

import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import py.com.prueba.modelo.Categoria;

/**
 *
 * @author root
 */
@Stateless
@LocalBean
public class CategoriaEJB {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    @PersistenceContext(unitName="pwbiiPU")
    private EntityManager em;
    protected EntityManager getEm() {
        return em;
    }

    public Categoria get(Integer id) {
        return em.find(Categoria.class, id);
    }
   @SuppressWarnings("unchecked")
    public List<Categoria> lista() {
        Query q = getEm().createQuery(
                "SELECT c FROM Categoria c");
        return (List<Categoria>) q.getResultList();
    }
   
   
    public Long total() {
        Query q = getEm().createQuery(
                "Select Count(c) from Categoria c");
        return (Long) q.getSingleResult();
    }
}
