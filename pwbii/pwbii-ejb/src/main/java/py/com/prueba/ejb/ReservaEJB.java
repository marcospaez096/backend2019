/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.prueba.ejb;
import javax.ws.rs.core.MultivaluedMap;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import org.apache.commons.collections.map.MultiValueMap;
import py.com.prueba.modelo.Agenda;
import py.com.prueba.modelo.Categoria;
import py.com.prueba.modelo.Local;
import py.com.prueba.modelo.Persona;
import py.com.prueba.modelo.Reserva;
import py.com.prueba.modelo.Servicio;
import py.com.prueba.modelo.Sucursal;
import py.com.prueba.modelo.SucursalServicio;


/**
 *
 * @author root
 */
@Stateless
@LocalBean
public class ReservaEJB {

  
    //Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    @PersistenceContext(unitName="pwbiiPU")
    private EntityManager em;
    protected EntityManager getEm() {
        return em;
    }

    public Reserva get(Integer id) {
        return em.find(Reserva.class, id);
        
    }
   
    public void persist(Reserva entity){
        
        entity.setFechaHoraCreacion(new Date());
        System.out.println("HORA INICIO :" + entity.getHoraInicio());
        getEm().persist(entity);
        System.out.println("Reserva creada");
    }
   @SuppressWarnings("unchecked")
   public List<Reserva> lista(Integer idSucursalServicio , Date fecha)  {
        
        Query q = getEm().createQuery(
                "SELECT r FROM Reserva r INNER JOIN r.idSucursalServicio w WHERE w.idSucursalServicio = :idSucursalServicio AND r.fecha = :stringFecha");
        q.setParameter("idSucursalServicio", idSucursalServicio);
      
        q.setParameter("stringFecha", fecha);
        return (List<Reserva>)q.getResultList();
    }
    
   public List<Reserva> filtrar(MultivaluedMap<String,String> queryParameters ){
          
          List<Reserva> lista = null;
          List<Reserva> w = new ArrayList<Reserva>();
          List<Reserva> b = new ArrayList<Reserva>();
          String query = "SELECT r FROM Reserva r";
          lista = (List<Reserva>)getEm().createQuery(query).getResultList();
          for(Reserva item : lista){
             
              try{
                boolean empleado = queryParameters.getFirst("idempleado")!=null ? item.getIdEmpleado().getIdPersona().equals(Integer.parseInt(queryParameters.getFirst("idempleado"))) : true;
                boolean sucursal = queryParameters.getFirst("idsucursal")!=null ? item.getIdSucursalServicio().getIdSucursal().getIdSucursal().equals(Integer.parseInt(queryParameters.getFirst("idsucursal"))) : true;
                boolean local =  queryParameters.getFirst("idlocal")!=null ? item.getIdSucursalServicio().getIdSucursal().getIdLocal() == Integer.parseInt(queryParameters.getFirst("idlocal")):true;
                boolean servicio = queryParameters.getFirst("idservicio")!=null ? item.getIdSucursalServicio().getIdServicio().getIdServicio().equals(Integer.parseInt(queryParameters.getFirst("idservicio"))):true;
                boolean asistio =  queryParameters.getFirst("asistio")!=null ? item.getFlagAsistio().equals(queryParameters.getFirst("asistio")) : true;
                boolean estado =  queryParameters.getFirst("estado")!=null ? item.getFlagEstado().equals(queryParameters.getFirst("estado")) : true;
                boolean fechadesde =  queryParameters.getFirst("fechadesde")!=null ? item.getFecha().compareTo(new SimpleDateFormat("dd-MM-yyyy").parse(queryParameters.getFirst("fechadesde"))) >=0 : true;
                boolean fechahasta = queryParameters.getFirst("fechahasta")!=null ? item.getFecha().compareTo(new SimpleDateFormat("dd-MM-yyyy").parse(queryParameters.getFirst("fechahasta"))) <=0 : true;
                if(empleado && sucursal && local && servicio && estado && asistio && fechadesde && fechahasta){
                    w.add(item);
                }
                
                if(queryParameters.getFirst("pagina")!=null && queryParameters.getFirst("por_pagina")!=null){
                   
                   int desde = Integer.parseInt(queryParameters.getFirst("pagina")) -1 ;
                   int hasta = Integer.parseInt(queryParameters.getFirst("por_pagina"))*desde + Integer.parseInt(queryParameters.getFirst("por_pagina"))-1;
                   System.out.println("DESDE -->" + desde + " hasta --->" + hasta);
                   if(desde==hasta){
                       b.add(w.get(desde));
                       System.out.println("TAMANO ---> "  + b.size());
                       return b;
                   }else{
                       return w.subList(desde, hasta);
                   }
                  
                  
                }
              }catch(Exception e){
                  System.out.println(e);
              }
          }

                
         
              
          return w;
          
    };     
 
   
    public Long total(Integer idSucursalServicio, Date fecha) {
        
        Query q = getEm().createQuery(
                "SELECT Count(r) FROM Reserva r INNER JOIN r.idSucursalServicio w WHERE w.idSucursalServicio = :idSucursalServicio AND r.fecha = :stringFecha");
        q.setParameter("idSucursalServicio",idSucursalServicio );
        q.setParameter("stringFecha", fecha);
        return (Long)q.getSingleResult();
    }
    
  
}
