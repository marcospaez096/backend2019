/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.prueba.ejb;

import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import py.com.prueba.modelo.Agenda;
import py.com.prueba.modelo.Especialidad;
import py.com.prueba.modelo.Persona;
import py.com.prueba.modelo.Sucursal;

/**
 *
 * @author root
 */
@Stateless
@LocalBean
public class EspecialidadEJB {

    @PersistenceContext(unitName="pwbiiPU")
    private EntityManager em;
    @Inject
    private EspecialidadEJB especialidadEJB;

    protected EntityManager getEm() {
        return em;
    }

    public Agenda get(Integer id) {
        return em.find(Agenda.class, id);
    }

    
    @SuppressWarnings("unchecked")
    public List<Especialidad> lista() {
        Query q = getEm().createQuery(
                "SELECT especialidades FROM Especialidad especialidades");
        return (List<Especialidad>) q.getResultList();
    }
    
    public List<Especialidad> getEspecialidad(Integer idSucursal){
       Query q = getEm().createQuery(
                "SELECT e FROM Especialidad e  INNER JOIN e.servicioList s INNER JOIN s.sucursalServicioList w INNER JOIN w.idSucursal z  WHERE z.idSucursal = :idSucursal"
              );
        
        System.out.println("Id sucursal ----> : " + idSucursal );
        q.setParameter("idSucursal", idSucursal);
        return (List<Especialidad>)q.getResultList();
    };
    
     public Long totalgetEspecialidad(Integer idSucursal){
      
    
        Query q = getEm().createQuery(
                                "SELECT Count(e) FROM  Especialidad e  INNER JOIN e.servicioList s INNER JOIN s.sucursalServicioList w INNER JOIN w.idSucursal z  WHERE z.idSucursal = :idSucursal"


        );
        System.out.println("Id sucursal ----> : " + idSucursal);
        q.setParameter("idSucursal", idSucursal);
        return (Long)q.getSingleResult();
        
    };
    
    
    public Long total() {
        Query q = getEm().createQuery(
                "Select Count(especialidades) from Especialidad especialidades");
        return (Long) q.getSingleResult();
    }
   
}
