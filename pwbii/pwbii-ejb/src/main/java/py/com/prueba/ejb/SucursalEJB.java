/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.prueba.ejb;

import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import py.com.prueba.modelo.Categoria;
import py.com.prueba.modelo.Sucursal;

/**
 *
 * @author root
 */
@Stateless
@LocalBean
public class SucursalEJB {

    //Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    @PersistenceContext(unitName="pwbiiPU")
    private EntityManager em;
    protected EntityManager getEm() {
        return em;
    }

    public Categoria get(Integer id) {
        return em.find(Categoria.class, id);
    }
   @SuppressWarnings("unchecked")
    public List<Sucursal> lista(Integer idCategoria) {
        Query q = getEm().createQuery(
                "SELECT s FROM Sucursal s INNER JOIN  s.local t INNER JOIN t.categoriaList z WHERE z.idCategoria = :idCategoria");
        q.setParameter("idCategoria", idCategoria);
        return (List<Sucursal>) q.getResultList();
    }
    
    
   
   
    public Long total(Integer idCategoria) {
        Query q = getEm().createQuery(
                                "SELECT Count(s) FROM Sucursal s INNER JOIN  s.local t INNER JOIN t.categoriaList z WHERE z.idCategoria = :idCategoria");
               q.setParameter("idCategoria", idCategoria);
        return (Long) q.getSingleResult();
    }
    
    public List<Sucursal> getSucursalesDisponibles(Integer idSucursalServicio){
        Query q = getEm().createQuery(
                
                "SELECT s FROM Sucursal s INNER JOIN s.sucursalServicioList w WHERE w.idSucursalServicio = :idSucursalServicio"
        
        );
        
        return (List<Sucursal>)q.setParameter("idSucursalServicio", idSucursalServicio).getResultList();
                
    };
}
