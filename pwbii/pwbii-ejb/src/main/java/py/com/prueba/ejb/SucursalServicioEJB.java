/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.prueba.ejb;

import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import py.com.prueba.modelo.Reserva;
import py.com.prueba.modelo.SucursalServicio;

/**
 *
 * @author root
 */
@Stateless
@LocalBean
public class SucursalServicioEJB {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    //Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    @PersistenceContext(unitName="pwbiiPU")
    private EntityManager em;
    protected EntityManager getEm() {
        return em;
    }

    public SucursalServicio get(Integer id) {
        return em.find(SucursalServicio.class, id);
    }
   @SuppressWarnings("unchecked")
   public SucursalServicio getSucursalServicio(Integer idSucursalServicio , Date fecha)  {
        
        Query q = getEm().createQuery(
                "SELECT ss FROM SucursalServicio ss WHERE ss.idSucursalServicio = :idSucursalServicio");
        q.setParameter("idSucursalServicio", idSucursalServicio);
        return (SucursalServicio)q.getSingleResult();
    }
   
   
    public Long total(Integer idSucursalServicio, Date fecha) {
        
        Query q = getEm().createQuery(
                "SELECT Count(ss) FROM SucursalServicio ss WHERE ss.idSucursalServicio = :idSucursalServicio");
        q.setParameter("idSucursalServicio",idSucursalServicio );
        return (Long)q.getSingleResult();
    }
}
