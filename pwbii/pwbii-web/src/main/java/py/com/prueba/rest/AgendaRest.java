package py.com.prueba.rest;

import py.com.prueba.ejb.AgendaEJB;
import py.com.prueba.modelo.Agenda;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.validation.Valid;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import org.apache.http.client.utils.DateUtils;
import org.hornetq.utils.json.JSONObject;
import org.hornetq.utils.json.JSONString;
import org.jboss.resteasy.spi.BadRequestException;
import py.com.prueba.ejb.CategoriaEJB;
import py.com.prueba.ejb.EspecialidadEJB;
import py.com.prueba.ejb.PersonaEJB;
import py.com.prueba.ejb.ReservaEJB;
import py.com.prueba.ejb.SucursalEJB;
import py.com.prueba.ejb.SucursalServicioEJB;
import py.com.prueba.modelo.Categoria;
import py.com.prueba.modelo.Especialidad;
import py.com.prueba.modelo.HorarioExcepcion;
import py.com.prueba.modelo.Local;
import py.com.prueba.modelo.Persona;
import py.com.prueba.modelo.Reserva;
import py.com.prueba.modelo.Sucursal;
import py.com.prueba.modelo.SucursalServicio;

@Path("agenda")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class AgendaRest {
    @Inject
    private AgendaEJB agendaEJB;
    @Inject
    private CategoriaEJB categoriaEJB;
    @Inject
    private  SucursalEJB sucursalEJB;
    @Inject
    private EspecialidadEJB especialidadEJB;
    @Inject
    private PersonaEJB personaEJB;
    @Inject
    private ReservaEJB reservaEJB;
    @Inject
    private SucursalServicioEJB sucursalServicioEJB;
    @Context
    protected UriInfo uriInfo;



    @GET
    @Path("/")
    public Response listar() throws WebApplicationException{

        List<Agenda> listEntity = null;
        Long total = null;
        total = agendaEJB.total();
        listEntity = agendaEJB.lista();
        Map<String,Object> mapaResultado=new HashMap<String, Object>();
        mapaResultado.put("total", total);
        mapaResultado.put("lista", listEntity);
        return Response.ok(mapaResultado).build();

    }

    @GET
    @Path("/{pk}")
    public Response obtener(@PathParam("pk") Integer pk) {
        Agenda entityRespuesta =null;
        entityRespuesta = agendaEJB.get(pk);
        return Response.ok(entityRespuesta).build();
    }
   
    @GET
    @Path("/sucursal")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSucursales(@QueryParam("idCategoria") Integer IdCategoria){
          List<Sucursal> listEntity = null;
        Long total = null;
        total = sucursalEJB.total(IdCategoria);
        listEntity = sucursalEJB.lista(IdCategoria);
        Map<String,Object> mapaResultado=new HashMap<>();
        mapaResultado.put("total", total);
        mapaResultado.put("listaSucursales", listEntity);
        System.out.println("Esta es la lista de categorias");
        System.out.println(listEntity);
        return Response.ok(mapaResultado).build();
        
    }
    
    @GET
    @Path("/categoria")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCategorias() {
        List<Categoria> listEntity = null;
        Long total = null;
        total = categoriaEJB.total();
        listEntity = categoriaEJB.lista();
        Map<String,Object> mapaResultado=new HashMap<>();
        mapaResultado.put("total", total);
        mapaResultado.put("listaCategorias", listEntity);
        System.out.println("Esta es la lista de categorias");
        System.out.println(listEntity);
        return Response.ok(mapaResultado).build();
    }
    @GET
    @Path("/especialidad")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEspecialidades(@QueryParam("idSucursal") Integer idSucursal){

        if(idSucursal!=null){
                    Long total =  null;
                    total = especialidadEJB.totalgetEspecialidad(idSucursal);
                    List<Especialidad> Entity = null;
                    Entity = especialidadEJB.getEspecialidad(idSucursal);
                    Map<String,Object> mapaResultado = new HashMap<>();
                    mapaResultado.put("total", total);
                    mapaResultado.put("Lista", Entity);
                             return Response.ok(mapaResultado).build();
            
        }else{
                    List<Especialidad> listEntity = null;
                    Long total = null;
                    total = especialidadEJB.total();
                    listEntity = especialidadEJB.lista();
                    Map<String,Object> mapaResultado = new HashMap<>();
                    mapaResultado.put("total", total);
                    mapaResultado.put("listaEspecialidades", listEntity);
                             return Response.ok(mapaResultado).build();
        }
        

           
        
        
    }
   
    @GET
    @Path("/profesional")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProfesionales(@QueryParam("idSucursalServicio") Integer idISucursalServicio){
        if(idISucursalServicio!=null){
                    Long total =  null;
                    total = personaEJB.getProfesionalTotal(idISucursalServicio);
                    List<Persona> Entity = null;
                    Entity = personaEJB.getProfesional(idISucursalServicio);
                    Map<String,Object> mapaResultado = new HashMap<>();
                    mapaResultado.put("total", total);
                    mapaResultado.put("Lista", Entity);
                    return Response.ok(mapaResultado).build();
            
        }else{
                    Long total =  null;
                    total = personaEJB.getProfesionalesTotal();
                    List<Persona> Entity = null;
                    Entity = personaEJB.getProfesionales();
                    Map<String,Object> mapaResultado = new HashMap<>();
                    mapaResultado.put("total", total);
                    mapaResultado.put("Lista", Entity);
                    return Response.ok(mapaResultado).build();
        }
        
            
        
        
        
        
                
    }
    
    
    @GET
    @Path("reserva/disponible")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getHorarios(@QueryParam("idSucursalServicio") Integer idISucursalServicio , @QueryParam("fecha") String fecha) throws ParseException{
     if(idISucursalServicio!=null){
        Long total =  null;
        total = sucursalServicioEJB.total(idISucursalServicio,new SimpleDateFormat("yyyy-MM-dd" , Locale.ENGLISH).parse(fecha));
        SucursalServicio Entity = sucursalServicioEJB.getSucursalServicio(idISucursalServicio,new SimpleDateFormat("yyyy-MM-dd" , Locale.ENGLISH).parse(fecha));
        Calendar c = Calendar.getInstance();
        c.setTime(new SimpleDateFormat("yyyy-MM-dd" , Locale.ENGLISH).parse(fecha));
        String dia = "";
        Date objDateInicio = null;
        Date objDateFin = null;
        //INICIALIZAR HASHMAP
        HashMap<String, HashMap<String,String>> Resultado = new HashMap<>();
        //Resultado.put("horarioExcepcion", "-");
        if("Mon".equals(c.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US))){
            dia = "lunes";
            objDateInicio = Entity.getIdSucursal().getLunesHoraApertura();
            objDateFin = Entity.getIdSucursal().getLunesHoraCierre();
            //Resultado.put("HorarioInicio", Entity.getIdSucursal().getLunesHoraApertura().toString());
            //Resultado.put("HorarioFin" ,  Entity.getIdSucursal().getLunesHoraCierre().toString());
        }else if("Tue".equals(c.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US))){
            dia = "martes";
            objDateInicio = Entity.getIdSucursal().getMartesHoraApertura();
            objDateFin = Entity.getIdSucursal().getMartesHoraCierre();
            //Resultado.put("HorarioInicio", Entity.getIdSucursal().getMartesHoraApertura().toString());
            //Resultado.put("HorarioFin" ,  Entity.getIdSucursal().getMartesHoraCierre().toString());
        }else if("Wed".equals(c.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US))){
            dia = "miercoles";
            objDateInicio = Entity.getIdSucursal().getMiercolesHoraApertura();
            objDateFin = Entity.getIdSucursal().getMiercolesHoraCierre();
            //Resultado.put("HorarioInicio", Entity.getIdSucursal().getMiercolesHoraApertura().toString());
            //Resultado.put("HorarioFin" ,  Entity.getIdSucursal().getMiercolesHoraCierre().toString());
        }else if("Thur".equals(c.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US))){
            dia = "jueves";
            objDateInicio = Entity.getIdSucursal().getJuevesHoraApertura();
            objDateFin = Entity.getIdSucursal().getJuevesHoraCierre();
            //Resultado.put("HorarioInicio", Entity.getIdSucursal().getJuevesHoraApertura().toString());
            //Resultado.put("HorarioFin" ,  Entity.getIdSucursal().getJuevesHoraCierre().toString());
        }else if("Fri".equals(c.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US))){
            dia="viernes";
            objDateInicio = Entity.getIdSucursal().getViernesHoraApertura();
            objDateFin = Entity.getIdSucursal().getViernesHoraCierre();
            //Resultado.put("HorarioInicio", Entity.getIdSucursal().getViernesHoraApertura().toString());
            //Resultado.put("HorarioFin" ,  Entity.getIdSucursal().getViernesHoraCierre().toString());
        }else if("Sat".equals(c.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US))){
            dia="sabado";
            objDateInicio = Entity.getIdSucursal().getSabadoHoraApertura();
            objDateFin = Entity.getIdSucursal().getSabadoHoraCierre();
            // Resultado.put("HorarioInicio", Entity.getIdSucursal().getSabadoHoraApertura().toString());
            //Resultado.put("HorarioFin" ,  Entity.getIdSucursal().getSabadoHoraCierre().toString());
        }else if("Sun".equals(c.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US))){
            dia="domingo";
            objDateInicio = Entity.getIdSucursal().getDomingoHoraApertura();
            objDateFin = Entity.getIdSucursal().getDomingoHoraCierre();
            //Resultado.put("HorarioInicio", Entity.getIdSucursal().getDomingoHoraApertura().toString());
            //Resultado.put("HorarioFin" ,  Entity.getIdSucursal().getDomingoHoraCierre().toString());
        }
        System.out.println("EL DIA ES : " + dia);
        System.out.println(c.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US));
        //PRIMERO VERIFICAR SI EXISTE UNA RESTRICCION DE HORARIO PARA ESA FECHA EN PARTICULAR;
        for(HorarioExcepcion horarioExcepcion : Entity.getIdSucursal().getHorarioExcepcionList()) {
                    if(horarioExcepcion.getFecha().equals(new SimpleDateFormat("yyyy-MM-dd" , Locale.ENGLISH).parse(fecha))){
                        System.out.println("Console");
                       // Resultado.put("horarioExcepcion", fecha);
                        objDateInicio = horarioExcepcion.getHoraApertura();
                        objDateFin = horarioExcepcion.getHoraCierre();
                        //Resultado.put("HorarioInicio",horarioExcepcion.getHoraApertura().toString());
                        //Resultado.put("HorarioInicio",horarioExcepcion.getHoraCierre().toString());

                        
                        
                    }
            
        }
        
        //PASO 2 : GENERAR AUTOMATICAMENTE LOS HORARIOS DISPONIBLES 
        Date a = objDateInicio;
        Date b = objDateFin;
        Calendar calIni = Calendar.getInstance();
        Calendar calFin = Calendar.getInstance();
        calIni.setTime(a);
        calFin.setTime(b);
        int i = 0 ;
        //GENERO TODOS LOS RESULTADOS
                     ArrayList<HashMap<String,String>> lista = new ArrayList<>();

        while(calIni.getTime().compareTo(calFin.getTime()) < 0){
              
              /*//SE IMPRIME EL TIEMPO EN CONSOLA
              HashMap<String,String> map = new HashMap();
              map.put("Reservado", "No");
               System.out.println("EL TIEMPO ES --> :" + calIni.getTime().toString());
               //PASO 3 : VERIFICAR SI EXISTE UNA RESERVA EN ESE TIEMPO
               for(Reserva reserva : Entity.getReservaList()) {
                 if(reserva.getFecha().toString().equals(fecha)){
                   //LA FECHA DE LA RESERVA COINCIDE CON LA FECHA PASADA EN LA URL
                     map.put("Reservado", "Si");
                     map.put("Fecha", fecha);
                     map.put("HoraInicio",new SimpleDateFormat("HH:mm:ss").format(calIni.getTime()));
                     
                   
                  }
               }
                      calIni.add(Calendar.MINUTE, Entity.getDuracion());
                      
                     Resultado.put("Registro Nro:" + i, map);
                     i++;
              */
              HashMap<String,String> t = new HashMap<>();
              t.put("Fecha", fecha);
              t.put("HoraInicio",new SimpleDateFormat("HH:mm:ss").format(calIni.getTime()));
              calIni.add(Calendar.MINUTE, Entity.getDuracion());
              t.put("HoraFin",new SimpleDateFormat("HH:mm:ss").format(calIni.getTime()));
              //SE AGREGA A LA LISTA
              lista.add(t);
        }
        System.out.println("LA LISTA ES : " );
        System.out.println(lista);
        
        //PASO 3 REMOVER LAS RESERVAS DE LOS HORARIOS DISPONIBLES DENTRO DEL ARRAYLIST
        for(Reserva reserva : Entity.getReservaList()) {
                if(reserva.getFecha().toString().equals(fecha)){
                    Iterator it = lista.iterator();
                    while(it.hasNext()){
                        HashMap<String,String> elemento = (HashMap<String,String>) it.next();
                         if(elemento.containsKey("HoraInicio") && elemento.containsValue(reserva.getHoraInicio().toString()) && elemento.containsKey("HoraFin") && elemento.containsValue(reserva.getHoraFin().toString())) {
                                    it.remove();
                                    System.out.println("Coincidencia");
                            }
                         }

                       
                    }
                     
          
                                    
                               
                          
                              
                           
                    
                }
                
                   
                  
        System.out.println("LA LISTA ES : " );
        System.out.println(lista);
        
        
        //VERIFICAR LA DURACION DEL SERVICIO
        //Resultado.put("DuracionServicio", Entity.getDuracion().toString());
        Map<String,Object> mapaResultado = new HashMap<>();
        mapaResultado.put("total", lista.size());
        mapaResultado.put("ListaDisponible", lista);
        
        
        
        return Response.ok(mapaResultado).build();
     }else{
         return Response.ok("Falta el idSucursalServicio o la fecha ").build();
     }
    }
    @POST
    @Path("/")
    public Response crear(Agenda entity) throws WebApplicationException {

        agendaEJB.persist(entity);

        UriBuilder resourcePathBuilder = UriBuilder.fromUri(uriInfo
                .getAbsolutePath());
        URI resourceUri=null;
        try {
            resourceUri = resourcePathBuilder
                    .path(URLEncoder.encode(entity.getIdAgenda().toString(), "UTF-8")).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.created(resourceUri).build();
    }
    
    
    @POST
    @Path("/reserva/crear")
    public Response crearReserva(Reserva entity) throws WebApplicationException{
                 

        reservaEJB.persist(entity);
        UriBuilder resourcePathBuilder = UriBuilder.fromUri(uriInfo
                .getAbsolutePath());
        URI resourceUri=null;
        try {
            resourceUri = resourcePathBuilder
                    .path(URLEncoder.encode(entity.getIdReserva().toString(), "UTF-8")).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.created(resourceUri).build();
   
       
    }
    
    @Path("/reserva")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public  Response filtrarReservas(@Context UriInfo uriInfo){
        
            //RECIBIR QUERYPARAMS DINAMICOS
            MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters(); 
            Map<String,Object> mapaResultado = new HashMap<>();
           reservaEJB.filtrar(queryParams);
           List<Reserva> ListEntity = reservaEJB.filtrar(queryParams);
           Long total  = null;
           mapaResultado.put("total", ListEntity.size());
           mapaResultado.put("Lista",ListEntity);
           return Response.ok(mapaResultado).header("Access-Control-Allow-Origin", "http://127.0.0.1:4200").build();

        
    }
   @OPTIONS
   @Path("/reserva")
    public Response getOptions() {
        return Response.ok()
             .header("Access-Control-Allow-Origin", "*")
             .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
             .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }
    @PUT
    @Path("/")
    public Response modificar(Agenda entity) throws WebApplicationException {
        agendaEJB.merge(entity);
        return Response.ok().build();
    }

    @DELETE
    @Path("/{pk}")
    public Response borrar(@PathParam("pk") Integer pk) throws WebApplicationException {

        agendaEJB.delete(pk);
        return Response.ok().build();

    }
}
