/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.prueba.modelo;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author root
 */
@Entity
@Table(name = "sucursal_servicio", catalog = "pwbii", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SucursalServicio.findAll", query = "SELECT s FROM SucursalServicio s")
    , @NamedQuery(name = "SucursalServicio.findByIdSucursalServicio", query = "SELECT s FROM SucursalServicio s WHERE s.idSucursalServicio = :idSucursalServicio")
    , @NamedQuery(name = "SucursalServicio.findByDuracion", query = "SELECT s FROM SucursalServicio s WHERE s.duracion = :duracion")
    , @NamedQuery(name = "SucursalServicio.findByPrecio", query = "SELECT s FROM SucursalServicio s WHERE s.precio = :precio")
    , @NamedQuery(name = "SucursalServicio.findByCapacidad", query = "SELECT s FROM SucursalServicio s WHERE s.capacidad = :capacidad")})
public class SucursalServicio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_sucursal_servicio")
    private Integer idSucursalServicio;
    @Column(name = "duracion")
    private Integer duracion;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "precio")
    @JsonIgnore
    private BigDecimal precio;
    @Basic(optional = false)
    @Column(name = "capacidad")
     private int capacidad;
    @JoinColumn(name = "id_servicio", referencedColumnName = "id_servicio")
    @ManyToOne(optional = false)
    //@JsonManagedReference
    private Servicio idServicio;
    @JoinColumn(name = "id_sucursal", referencedColumnName = "id_sucursal")
    @ManyToOne(optional = false)
    private Sucursal idSucursal;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSucursalServicio")
    @JsonIgnore
    private List<PersonaSucursalServicio> personaSucursalServicioList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSucursalServicio" , fetch =  FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonIgnore
    private List<Reserva> reservaList;
    public SucursalServicio() {
    }

    public SucursalServicio(Integer idSucursalServicio) {
        this.idSucursalServicio = idSucursalServicio;
    }

    public SucursalServicio(Integer idSucursalServicio, BigDecimal precio, int capacidad) {
        this.idSucursalServicio = idSucursalServicio;
        this.precio = precio;
        this.capacidad = capacidad;
    }

    public Integer getIdSucursalServicio() {
        return idSucursalServicio;
    }

    public void setIdSucursalServicio(Integer idSucursalServicio) {
        this.idSucursalServicio = idSucursalServicio;
    }

    public Integer getDuracion() {
        return duracion;
    }

    public void setDuracion(Integer duracion) {
        this.duracion = duracion;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public Servicio getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Servicio idServicio) {
        this.idServicio = idServicio;
    }

    public Sucursal getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(Sucursal idSucursal) {
        this.idSucursal = idSucursal;
    }

    @XmlTransient
    public List<PersonaSucursalServicio> getPersonaSucursalServicioList() {
        return personaSucursalServicioList;
    }

    public void setPersonaSucursalServicioList(List<PersonaSucursalServicio> personaSucursalServicioList) {
        this.personaSucursalServicioList = personaSucursalServicioList;
    }

    @XmlTransient
    public List<Reserva> getReservaList() {
        return reservaList;
    }

    public void setReservaList(List<Reserva> reservaList) {
        this.reservaList = reservaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSucursalServicio != null ? idSucursalServicio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SucursalServicio)) {
            return false;
        }
        SucursalServicio other = (SucursalServicio) object;
        if ((this.idSucursalServicio == null && other.idSucursalServicio != null) || (this.idSucursalServicio != null && !this.idSucursalServicio.equals(other.idSucursalServicio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.prueba.modelo.SucursalServicio[ idSucursalServicio=" + idSucursalServicio + " ]";
    }
    
}
