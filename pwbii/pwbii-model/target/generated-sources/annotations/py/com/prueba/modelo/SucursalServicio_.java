package py.com.prueba.modelo;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import py.com.prueba.modelo.PersonaSucursalServicio;
import py.com.prueba.modelo.Reserva;
import py.com.prueba.modelo.Servicio;
import py.com.prueba.modelo.Sucursal;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-07T12:48:36")
@StaticMetamodel(SucursalServicio.class)
public class SucursalServicio_ { 

    public static volatile SingularAttribute<SucursalServicio, Sucursal> idSucursal;
    public static volatile SingularAttribute<SucursalServicio, Integer> idSucursalServicio;
    public static volatile SingularAttribute<SucursalServicio, BigDecimal> precio;
    public static volatile ListAttribute<SucursalServicio, PersonaSucursalServicio> personaSucursalServicioList;
    public static volatile ListAttribute<SucursalServicio, Reserva> reservaList;
    public static volatile SingularAttribute<SucursalServicio, Integer> duracion;
    public static volatile SingularAttribute<SucursalServicio, Servicio> idServicio;
    public static volatile SingularAttribute<SucursalServicio, Integer> capacidad;

}