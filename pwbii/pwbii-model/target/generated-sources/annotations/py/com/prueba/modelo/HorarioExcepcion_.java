package py.com.prueba.modelo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import py.com.prueba.modelo.Sucursal;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-07T12:48:36")
@StaticMetamodel(HorarioExcepcion.class)
public class HorarioExcepcion_ { 

    public static volatile SingularAttribute<HorarioExcepcion, Sucursal> idSucursal;
    public static volatile SingularAttribute<HorarioExcepcion, Date> fecha;
    public static volatile SingularAttribute<HorarioExcepcion, Date> horaApertura;
    public static volatile SingularAttribute<HorarioExcepcion, Integer> idHorarioExcepcion;
    public static volatile SingularAttribute<HorarioExcepcion, Date> horaCierre;

}