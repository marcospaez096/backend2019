package py.com.prueba.modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import py.com.prueba.modelo.Especialidad;
import py.com.prueba.modelo.SucursalServicio;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-07T12:48:36")
@StaticMetamodel(Servicio.class)
public class Servicio_ { 

    public static volatile SingularAttribute<Servicio, Integer> duracionReferencia;
    public static volatile SingularAttribute<Servicio, Integer> idServicio;
    public static volatile ListAttribute<Servicio, SucursalServicio> sucursalServicioList;
    public static volatile SingularAttribute<Servicio, Especialidad> idEspecialidad;
    public static volatile SingularAttribute<Servicio, String> nombre;

}