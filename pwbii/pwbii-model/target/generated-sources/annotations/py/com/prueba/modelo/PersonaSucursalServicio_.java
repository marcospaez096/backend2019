package py.com.prueba.modelo;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import py.com.prueba.modelo.Persona;
import py.com.prueba.modelo.SucursalServicio;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-07T12:48:36")
@StaticMetamodel(PersonaSucursalServicio.class)
public class PersonaSucursalServicio_ { 

    public static volatile SingularAttribute<PersonaSucursalServicio, BigDecimal> precio;
    public static volatile SingularAttribute<PersonaSucursalServicio, SucursalServicio> idSucursalServicio;
    public static volatile SingularAttribute<PersonaSucursalServicio, Persona> idEmpleado;
    public static volatile SingularAttribute<PersonaSucursalServicio, Integer> idPersonaSucursalServicio;

}