package py.com.prueba.modelo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import py.com.prueba.modelo.Ciudad;
import py.com.prueba.modelo.HorarioExcepcion;
import py.com.prueba.modelo.Local;
import py.com.prueba.modelo.Mapa;
import py.com.prueba.modelo.SucursalServicio;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-07T12:48:36")
@StaticMetamodel(Sucursal.class)
public class Sucursal_ { 

    public static volatile SingularAttribute<Sucursal, String> descripcion;
    public static volatile SingularAttribute<Sucursal, Date> lunesHoraCierre;
    public static volatile SingularAttribute<Sucursal, Date> juevesHoraCierre;
    public static volatile SingularAttribute<Sucursal, Date> lunesHoraApertura;
    public static volatile SingularAttribute<Sucursal, Integer> idLocal;
    public static volatile SingularAttribute<Sucursal, Date> viernesHoraApertura;
    public static volatile SingularAttribute<Sucursal, Date> miercolesHoraApertura;
    public static volatile SingularAttribute<Sucursal, Date> sabadoHoraApertura;
    public static volatile ListAttribute<Sucursal, SucursalServicio> sucursalServicioList;
    public static volatile SingularAttribute<Sucursal, String> nombre;
    public static volatile SingularAttribute<Sucursal, Date> viernesHoraCierre;
    public static volatile SingularAttribute<Sucursal, Ciudad> idCiudad;
    public static volatile SingularAttribute<Sucursal, Local> local;
    public static volatile SingularAttribute<Sucursal, Integer> idSucursal;
    public static volatile SingularAttribute<Sucursal, Date> martesHoraApertura;
    public static volatile SingularAttribute<Sucursal, Mapa> idMapa;
    public static volatile SingularAttribute<Sucursal, Date> sabadoHoraCierre;
    public static volatile SingularAttribute<Sucursal, Date> martesHoraCierre;
    public static volatile SingularAttribute<Sucursal, Date> domingoHoraApertura;
    public static volatile SingularAttribute<Sucursal, Date> juevesHoraApertura;
    public static volatile SingularAttribute<Sucursal, Date> miercolesHoraCierre;
    public static volatile SingularAttribute<Sucursal, Date> domingoHoraCierre;
    public static volatile ListAttribute<Sucursal, HorarioExcepcion> horarioExcepcionList;

}