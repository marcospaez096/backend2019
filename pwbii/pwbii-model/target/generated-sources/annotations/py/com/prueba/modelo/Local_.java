package py.com.prueba.modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import py.com.prueba.modelo.Categoria;
import py.com.prueba.modelo.Persona;
import py.com.prueba.modelo.Sucursal;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-07T12:48:36")
@StaticMetamodel(Local.class)
public class Local_ { 

    public static volatile SingularAttribute<Local, String> descripcion;
    public static volatile SingularAttribute<Local, Integer> idLocal;
    public static volatile ListAttribute<Local, Persona> personaList;
    public static volatile SingularAttribute<Local, Sucursal> sucursal;
    public static volatile SingularAttribute<Local, String> nombre;
    public static volatile ListAttribute<Local, Categoria> categoriaList;

}