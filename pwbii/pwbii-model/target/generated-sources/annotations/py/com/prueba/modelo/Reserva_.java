package py.com.prueba.modelo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import py.com.prueba.modelo.Persona;
import py.com.prueba.modelo.SucursalServicio;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-07T12:48:36")
@StaticMetamodel(Reserva.class)
public class Reserva_ { 

    public static volatile SingularAttribute<Reserva, Date> horaFin;
    public static volatile SingularAttribute<Reserva, String> flagAsistio;
    public static volatile SingularAttribute<Reserva, Date> fecha;
    public static volatile SingularAttribute<Reserva, SucursalServicio> idSucursalServicio;
    public static volatile SingularAttribute<Reserva, Persona> idCliente;
    public static volatile SingularAttribute<Reserva, Persona> idEmpleado;
    public static volatile SingularAttribute<Reserva, Date> fechaHoraCreacion;
    public static volatile SingularAttribute<Reserva, String> flagEstado;
    public static volatile SingularAttribute<Reserva, Integer> idReserva;
    public static volatile SingularAttribute<Reserva, Date> horaInicio;
    public static volatile SingularAttribute<Reserva, String> observacion;

}