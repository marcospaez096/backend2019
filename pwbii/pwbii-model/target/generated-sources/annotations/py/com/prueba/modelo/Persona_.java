package py.com.prueba.modelo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import py.com.prueba.modelo.Agenda;
import py.com.prueba.modelo.Local;
import py.com.prueba.modelo.PersonaSucursalServicio;
import py.com.prueba.modelo.Reserva;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-07T12:48:36")
@StaticMetamodel(Persona.class)
public class Persona_ { 

    public static volatile ListAttribute<Persona, Reserva> reservaList1;
    public static volatile SingularAttribute<Persona, Date> fechaNacimiento;
    public static volatile SingularAttribute<Persona, Local> idLocal;
    public static volatile ListAttribute<Persona, Agenda> agendaList;
    public static volatile SingularAttribute<Persona, String> nombre;
    public static volatile SingularAttribute<Persona, String> flagEmpleado;
    public static volatile ListAttribute<Persona, PersonaSucursalServicio> personaSucursalServicioList;
    public static volatile ListAttribute<Persona, Reserva> reservaList;
    public static volatile SingularAttribute<Persona, String> apellido;
    public static volatile SingularAttribute<Persona, String> usuario;
    public static volatile SingularAttribute<Persona, String> numeroDocumento;
    public static volatile SingularAttribute<Persona, Integer> idPersona;
    public static volatile SingularAttribute<Persona, String> email;

}